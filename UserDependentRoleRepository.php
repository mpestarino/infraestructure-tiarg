<?php

namespace App\Repository;

use App\Entity\UserDependentRole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserDependentRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserDependentRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserDependentRole[]    findAll()
 * @method UserDependentRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserDependentRoleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserDependentRole::class);
    }

    // /**
    //  * @return UserDependentRole[] Returns an array of UserDependentRole objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserDependentRole
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
