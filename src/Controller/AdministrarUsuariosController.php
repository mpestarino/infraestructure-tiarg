<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Recursos;
use App\Entity\User;
use App\Repository\RecursosRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_ADMIN")
 */
class AdministrarUsuariosController extends AbstractController
{
    /**
     * @Route("admusuarios", name="administrar_usuarios")
     */
    public function lista(Request $request)    {

        $recursos = $this->getDoctrine()->getRepository(Recursos::class)->findAll();
        $user = $this->getDoctrine()->getRepository(User::class)->findAll();


        return $this->render('administrar_usuarios/index.html.twig', [
            'recursos' => $recursos,
            'User' => $user
        ]);
    }
}
