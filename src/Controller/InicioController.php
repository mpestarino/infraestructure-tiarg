<?php

namespace App\Controller;

use App\Repository\RecursosRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class InicioController extends AbstractController
{
    /**
     * @Route("/inicio", name="inicio")
     */
    public function lista()
    {      

        return $this->render('inicio/inicio.html.twig', [
            'controller_name' => 'RecursosController',]);
            

    }
    
    

}

