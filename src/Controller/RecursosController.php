<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EntityType;
use App\Repository\RecursosRepository;
use App\Form\RecursosType;
use App\Entity\Recursos;
use App\Entity\User;
use App\Entity\Categorias;
use App\Repository\UserRepository;
use App\Repository\CategoriasRepository;
use Doctrine\ORM\EntityManager;


class RecursosController extends AbstractController
{
    /**
     * @Route("/recursos", name="recursos_lista")
     */
    public function lista(Request $request)    {

        $recursos = $this->getDoctrine()->getRepository(Recursos::class)->findAll();

        return $this->render('recursos/index.html.twig', [
            'recursos' => $recursos,
        ]);
    }
    /**
     * @Route("/recursos/agregar", name="recursos_agregar")
     */
    public function agregar(Request $request)    {
        $recursos = new Recursos;
        $user = $this->getDoctrine()->getRepository(User::class)->findAll();
        $categorias = $this->getDoctrine()->getRepository(categorias::class)->findAll();
        $form = $this->createFormBuilder($recursos)

            ->add('Nombre', TextType::class)
            ->add('Categoria', ChoiceType::class, array(
                'choices'=>array($categorias),

                'choice_label'=>function($categorias, $key){
                },
            ))
            ->add('email', ChoiceType::class, array(
                'choices'=>array($user),

                'choice_label'=>function($user, $key){
                },
            ))
        
            ->add('Guardar', SubmitType::class, ['label' => 'Crear Recurso'])
                
            ->getForm();
         
            
            $form->handleRequest($request);


            if ($form->isSubmitted() && $form->isValid()) {
             
                $recursos = $form->getData();

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($recursos);
                $entityManager->flush();

                return $this->render('recursos/guardado.html.twig');
            };
            return $this->render('recursos/agregar.html.twig', [
                'form' => $form->createView(),
                ]);
    }
    /**
     * @Route("/recursos/editar/{id}", name="recursos_editar", methods={"GET","POST"})
     */
    public function editar(Request $request, $id)    {
        $em = $this->getDoctrine()->getManager();
        $recursos = $em->getRepository(Recursos::class)->find($id);
        $user = $this->getDoctrine()->getRepository(User::class)->findAll();
        $categorias = $this->getDoctrine()->getRepository(categorias::class)->findAll();
        $form = $this->createFormBuilder($recursos)
    
            ->add('Nombre', TextType::class)
            ->add('Categoria', ChoiceType::class, array(
                'choices'=>array($categorias),
                'choice_label'=>function($categorias){
                },
            ))
            ->add('email', ChoiceType::class, array(
                'choices'=>array($user),
                'choice_label'=>function($user, $key){
                },
            ))
            ->add('Guardar', SubmitType::class, ['label' => 'Crear Recurso'])  
            ->getForm();
            
            $form->handleRequest($request); 

            if ($form->isSubmitted() && $form->isValid()) {
             
                $recursos = $form->getData();

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($recursos, $user, $categorias);
                $entityManager->flush();
            

                return $this->render('recursos/guardado.html.twig');
            };
            return $this->render('recursos/agregar.html.twig', [
                'form' => $form->createView(),
                ]);
    }
    /**
     * @Route("/recursos/eliminar/{id}", name="recursos_eliminar")
     */
    public function eliminar($id)    {
        
        $entityManager = $this->getDoctrine()->getManager();
        $recursos = $entityManager->getRepository(Recursos::class)->find($id);
    
        $entityManager->remove($recursos);
        $entityManager->flush();
    
        return $this->render('recursos/eliminar.html.twig');
    }

}



