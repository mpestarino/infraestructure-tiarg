<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fullname;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Recursos", mappedBy="email")
     */
    private $email_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\recursos", inversedBy="recurso")
     */
    private $recurso;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $pedir;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $categoria;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    public function __construct()
    {
        $this->email_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    public function setFullname(string $fullname): self
    {
        $this->fullname = $fullname;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return (Role|string)[] 
     */
    public function getRoles()
{
    $roles = $this->roles;

    $roles[] = 'ROLE_USER';

    return array_unique($roles);
}

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return null;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        return null;
    }

    /**
     * @return Collection|Recursos[]
     */
    public function getEmailId(): Collection
    {
        return $this->email_id;
    }

    public function addEmailId(Recursos $emailId): self
    {
        if (!$this->email_id->contains($emailId)) {
            $this->email_id[] = $emailId;
            $emailId->setEmail($this);
        }

        return $this;
    }

    public function removeEmailId(Recursos $emailId): self
    {
        if ($this->email_id->contains($emailId)) {
            $this->email_id->removeElement($emailId);
            // set the owning side to null (unless already changed)
            if ($emailId->getEmail() === $this) {
                $emailId->setEmail(null);
            }
        }

        return $this;
    }

    public function setRoles(array $roles)
    {
        $this->roles = $roles;
    }

    public function getRecurso(): ?recursos
    {
        return $this->recurso;
    }

    public function setRecurso(?recursos $recurso): self
    {
        $this->recurso = $recurso;

        return $this;
    }

    public function getPedir(): ?string
    {
        return $this->pedir;
    }

    public function setPedir(string $pedir): self
    {
        $this->pedir = $pedir;

        return $this;
    }

    public function getCategoria(): ?string
    {
        return $this->categoria;
    }

    public function setCategoria(string $categoria): self
    {
        $this->categoria = $categoria;

        return $this;
    }

}