<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RecursosRepository")
 */
class Recursos
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $Nombre;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $Fecha_devolucion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categorias", inversedBy="categorias")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Categoria;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="email_id")
     * @ORM\JoinColumn(nullable=true)
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="recurso")
     */
    private $recurso;

    public function __construct()
    {
        $this->recurso = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->Nombre;
    }

    public function setNombre(string $Nombre): self
    {
        $this->Nombre = $Nombre;

        return $this;
    }

    public function getFechaDevolucion(): ?\DateTimeInterface
    {
        return $this->Fecha_devolucion;
    }

    public function setFechaDevolucion(?\DateTimeInterface $Fecha_devolucion): self
    {
        $this->Fecha_devolucion = $Fecha_devolucion;

        return $this;
    }

    public function getCategoria(): ?Categorias
    {
        return $this->Categoria;
    }

    public function setCategoria(?Categorias $Categoria): self
    {
        $this->Categoria = $Categoria;

        return $this;
    }

    public function getEmail(): ?User
    {
        return $this->email;
    }

    public function setEmail(?User $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getRecurso(): Collection
    {
        return $this->recurso;
    }

    public function addRecurso(User $recurso): self
    {
        if (!$this->recurso->contains($recurso)) {
            $this->recurso[] = $recurso;
            $recurso->setRecurso($this);
        }

        return $this;
    }

    public function removeRecurso(User $recurso): self
    {
        if ($this->recurso->contains($recurso)) {
            $this->recurso->removeElement($recurso);
            // set the owning side to null (unless already changed)
            if ($recurso->getRecurso() === $this) {
                $recurso->setRecurso(null);
            }
        }

        return $this;
    }

}
